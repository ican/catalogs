vgrid_locations = [
    "/work/mh0287/m300083/quickplots/dpp0016_atm_vgrid_ml.nc",
    "/work/mh0287/m300083/experiments/dpp0066/dpp0066_atm_vgrid_ml.nc",
    "/work/mh0287/m300083/experiments/dpp0067/dpp0067_atm_vgrid_ml.nc",
]

import xarray as xr
import yaml

def np2py(e):
    try:
        return e.item()
    except ValueError:
        return e.tolist()
    except AttributeError:
        return e

def main():
    vgrid_sources = {}
    for fn in vgrid_locations:
        try:
            ds = xr.open_dataset(fn)
        except:
            print(f"WARNING: can't open {fn}")
            continue
        metadata = {k: np2py(v) for k, v in ds.attrs.items()}
        uuid = metadata["uuidOfVGrid"]
        vgrid_sources[uuid] = {
            "args": {
                "urlpath": fn,
            },
            "driver": "netcdf",
            "metadata": metadata,
        }
    cat = {"sources": vgrid_sources}
    print(yaml.dump(cat))

if __name__ == "__main__":
    main()
