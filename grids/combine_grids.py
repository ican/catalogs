import yaml

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("infiles", nargs="+")
    parser.add_argument("-o", "--outfile")
    args = parser.parse_args()
    
    sources = {
        k: v
        for fn in args.infiles
        for k, v in yaml.load(open(fn), Loader=yaml.SafeLoader)["sources"].items()
    }
    
    with open(args.outfile, "w") as of:
        yaml.dump({"sources": sources}, of)

if __name__ == "__main__":
    main()