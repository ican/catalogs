# climate model intake catalogs

This repository contains climate model intake catalogs.

## usage

```python
import intake
cat = intake.open_catalog("catalog.yml")
```

(you might have to specify an abosolute path to this entry point)

### getting model output

The catalog is organized by projects and model runs, you'll get data like:

```
data = cat.nextGEMS.dpp0066["2d_ml"].to_dask()
```

If you don't know where to look, you can also list contents:

```
list(cat)
list(cat.nextGEMS)
```

or search:

```
cat.search("dpp0066")
```

### getting grids

grids can be found by UUID, which are part of the model output:

```
grid = cat.grids.by_uuid[data.uuidOfHGrid].to_dask()
```
